### Contact Information
Author: Jared Knofczynski, jknofczy@uoregon.edu

### Running the program

* After cloning this repository, navigate to the `/web/` directory.

* Build the app image using

  ```
  docker build -t jknofczy-proj2 .
  ```

* Run the container using

  ```
  docker run -dp 5000:5000 jknofczy-proj2
  ```

* (Or run both commands together by running `./run.sh`)

* Navigate to `localhost:5000/trivia.html` in your web browser and check the output.

* Run automated tests from the root directory using ```./tests.sh localhost:5000``` from another terminal window while the app is running.
