import flask # needed to call render_template

PORT = 5000 # default port is 5000

app = flask.Flask(__name__)

@app.route("/")
def index():
    return "Index Page"

forbidden_chars = ["//", "~", ".."]

@app.route("/<name>") # if page exists, load it from templates directory
def load_page(name):
    url = flask.request.__dict__['environ']['REQUEST_URI'] # access page file path
    for char in forbidden_chars: # raise error 403 if applicable
        if char in url:
            flask.abort(403)

    if not name.endswith((".html", ".css")): # only allow .html and .css files
        flask.abort(403)

    try:
        with open(f"templates/{name}") as f:
            return flask.render_template(f'{name}')
    except:
        flask.abort(404)

@app.errorhandler(403)
def page_is_forbidden(error):
    return flask.render_template('403.html'), 403

@app.errorhandler(404)
def page_not_found(error):
    url = flask.request.__dict__['environ']['REQUEST_URI'] # access page file path
    for char in forbidden_chars: # prioritize error 403 over error 404
        if char in url:
            # flask.abort(403) # gives error when loading http://localhost:5000/path/to/~file.html
            return flask.render_template('403.html'), 403
    return flask.render_template('404.html'), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0', port=PORT)
